import { Component, OnInit } from '@angular/core';

import { Article } from '../models/models';
import { informed } from '../../assets/json/data.json';

@Component({
  selector: 'app-informed',
  templateUrl: './informed.component.html',
  styleUrls: ['./informed.component.css'],
})
export class InformedComponent implements OnInit {
  public info: Array<Article> = [];

  ngOnInit(): void {
    this.info = informed;
  }
}
