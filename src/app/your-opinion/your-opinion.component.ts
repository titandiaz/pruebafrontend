import { Component, OnInit, Input } from '@angular/core';

import { Opinion, Card } from './../models/models';
import { sectionOpinion } from '../../assets/json/data.json';

@Component({
  selector: 'app-your-opinion',
  templateUrl: './your-opinion.component.html',
  styleUrls: ['./your-opinion.component.css'],
})
export class YourOpinionComponent implements OnInit {
  public opinions: Array<Opinion> = [];
  public opinionCard: Card;
  @Input() textArea = '';

  ngOnInit(): void {
    this.opinions = sectionOpinion.opinions;
    this.opinionCard = sectionOpinion.card;
  }
  public changeArea(message): void {
    this.textArea = message;
  }
}
