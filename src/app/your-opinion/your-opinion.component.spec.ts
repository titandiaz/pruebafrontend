import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YourOpinionComponent } from './your-opinion.component';

describe('YourOpinionComponent', () => {
  let component: YourOpinionComponent;
  let fixture: ComponentFixture<YourOpinionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YourOpinionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YourOpinionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
