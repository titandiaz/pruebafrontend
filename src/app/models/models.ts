export interface Card {
  title: string;
  subtitle: string;
  description: string;
}
export interface Article {
  date: string;
  description: string;
  image: string;
}
export interface Opinion {
  type: string;
  badge: string;
  title: string;
  description: string;
  members: string;
}
export interface Theme {
  title: string;
  description: string;
  image: string;
  redirect: Redirect;
}
export interface Redirect {
  path: string;
  name: string;
}
