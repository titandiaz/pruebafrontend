import { Component, OnInit, Input } from '@angular/core';

import { Opinion } from '../../models/models';

@Component({
  selector: 'app-opinion-event',
  templateUrl: './opinion-event.component.html',
  styleUrls: ['./opinion-event.component.css'],
})
export class OpinionEventComponent implements OnInit {
  @Input() opinion: Opinion;
  @Input() color;

  ngOnInit(): void {
    switch (this.opinion.type) {
      case 'warning':
        this.color = { 'background-color': '#F3561F' };
        break;
      case 'success':
        this.color = { 'background-color': '#069169' };
        break;
      case 'info':
        this.color = { 'background-color': '#3366CC' };
        break;
      default:
        break;
    }
  }
}
