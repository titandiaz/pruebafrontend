import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpinionEventComponent } from './opinion-event.component';

describe('OpinionEventComponent', () => {
  let component: OpinionEventComponent;
  let fixture: ComponentFixture<OpinionEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpinionEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpinionEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
