import { Component, Input } from '@angular/core';

import { Theme } from '../../models/models';

@Component({
  selector: 'app-card-interest',
  templateUrl: './card-interest.component.html',
  styleUrls: ['./card-interest.component.css'],
})
export class CardInterestComponent {
  @Input() card: Theme;
}
