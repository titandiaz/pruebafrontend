import { Component, OnInit } from '@angular/core';

import { Theme } from '../models/models';
import { themes } from '../../assets/json/data.json';

@Component({
  selector: 'app-interest',
  templateUrl: './interest.component.html',
  styleUrls: ['./interest.component.css'],
})
export class InterestComponent implements OnInit {
  public cards: Array<Theme> = [];

  ngOnInit(): void {
    this.cards = themes;
  }
}
