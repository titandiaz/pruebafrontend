import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { InterestComponent } from './interest/interest.component';
import { CardInterestComponent } from './components/card-interest/card-interest.component';
import { YourOpinionComponent } from './your-opinion/your-opinion.component';
import { OpinionEventComponent } from './components/opinion-event/opinion-event.component';
import { InformedComponent } from './informed/informed.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    InterestComponent,
    CardInterestComponent,
    YourOpinionComponent,
    OpinionEventComponent,
    InformedComponent,
  ],
  imports: [BrowserModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
