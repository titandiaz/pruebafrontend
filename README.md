# [PruebaFrontEnd](https://prueba-frontend.netlify.app/)

Vista Home desarrollada en Angular 10, basado en el diseño del Landing page [GOV.CO](https://xd.adobe.com/spec/db4d4481-94e5-49ad-459a-0b0b0adfaa30-ad7c/) creado con [Adobe XD](https://www.adobe.com/la/products/xd.html),

Las secciones de diseño implementadas fueron:

- Queremos conocer tu opinión
- Infórmate
- Otros temas de interés

[Demo](https://prueba-frontend.netlify.app/)

[![Vista previa](https://gitlab.com/titandiaz/pruebafrontend/-/raw/master/src/assets/img/screen.png)](https://prueba-frontend.netlify.app/)

## Tecnologías y librerías usadas

- [Angular CLI](https://github.com/angular/angular-cli) version 10.0.5.
- [Bootstrap](https://getbootstrap.com/docs/4.3/getting-started/introduction/) version 4.3.1
- [CDN GOV](https://cdn.www.gov.co/v2/pages/cards)
- HTML
- CSS

## Requerimientos

- Git
- Node
- Npm
- Visual Studio Code

## Clonar el respositorio

```
git clone https://gitlab.com/titandiaz/pruebafrontend
```

```
cd pruebaFrontEnd
npm install
ng serve -o
```
